package com.gabrielguzman.android.torchdeliveryfordrivers;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;

/**
 * Custom adapter to show the Extra Info icons and descriptions in the RecyclerView at ShipmentAdapter
 * @author Gabriel Guzman
 * @since 2016.08.06
 * @see android.support.v7.widget.RecyclerView.Adapter
 */
public class ExtraInfoAdapter extends RecyclerView.Adapter<ExtraInfoAdapter.ExtraInfoHolder> {

    private  ArrayList<String> searchArrayList = new ArrayList<>();
    private  Context mContext;
    private  int[] eiImgArray = {R.mipmap.ei_liftage,R.mipmap.ei_residential,R.mipmap.ei_extra_person,
                                 R.mipmap.ei_breakdown,R.mipmap.ei_garbage_disposal, R.mipmap.ei_inside,
                                 R.mipmap.ei_flatbed, R.mipmap.ei_roller_bed, R.mipmap.ei_air_ride,
                                 R.mipmap.ei_other}; //IDs for every icon
    private String othDescp;

    public ExtraInfoAdapter(Shipment shipment, Context actContext) {

        //This builds a list with all the EI parameters contained in the selected shipment
        if (shipment.getLiftGate()==1)
            this.searchArrayList.add("LG");
        if (shipment.getResidential()==1)
            this.searchArrayList.add("RE");
        if (shipment.getExtraPerson()==1)
            this.searchArrayList.add("EP");
        if (shipment.getBreakdown()==1)
            this.searchArrayList.add("BD");
        if (shipment.getGarbageDisposal()==1)
            this.searchArrayList.add("GD");
        if (shipment.getInside()==1)
            this.searchArrayList.add("IN");
        if (shipment.getFlatbed()==1)
            this.searchArrayList.add("FB");
        if (shipment.getRollerBed()==1)
            this.searchArrayList.add("RB");
        if (shipment.getAirRide()==1)
            this.searchArrayList.add("AR");
        if (!shipment.getOther().equals("")) {
            this.searchArrayList.add("OT");
            othDescp = shipment.getOther();
        }
        this.mContext = actContext;
    }

    @Override
    public ExtraInfoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.extra_info_item, parent,false);
        return new ExtraInfoHolder(v);
    }

    @Override
    public void onBindViewHolder(final ExtraInfoHolder holder, int position) {
        String item = searchArrayList.get(position);

        switch (item) {
            case "LG":
                setExtraInfoItem(holder.imgReqIcon, eiImgArray[0], " Lifgate ");
                break;
            case "RE":
                setExtraInfoItem(holder.imgReqIcon, eiImgArray[1], " Residential ");
                break;
            case "EP":
                setExtraInfoItem(holder.imgReqIcon, eiImgArray[2], " Extra Person ");
                break;
            case "BD":
                setExtraInfoItem(holder.imgReqIcon, eiImgArray[3], " Breakdown ");
                break;
            case "GD":
                setExtraInfoItem(holder.imgReqIcon, eiImgArray[4], " Garbage Disposal ");
                break;
            case "IN":
                setExtraInfoItem(holder.imgReqIcon, eiImgArray[5], " Inside ");
                break;
            case "FB":
                setExtraInfoItem(holder.imgReqIcon, eiImgArray[6], " Flatbed ");
                break;
            case "RB":
                setExtraInfoItem(holder.imgReqIcon, eiImgArray[7], " Rollerbed ");
                break;
            case "AR":
                setExtraInfoItem(holder.imgReqIcon, eiImgArray[8], " Air Ride ");
                break;
            case "OT":
                setExtraInfoItem(holder.imgReqIcon, eiImgArray[9], othDescp);
                break;
        }

        holder.itemView.setTag(item);
    }

    @Override
    public int getItemCount() {
        return searchArrayList.size();
    }

    public static class ExtraInfoHolder extends RecyclerView.ViewHolder {
        protected ImageView imgReqIcon;
        protected TextView txtReqDescp;

        private ExtraInfoHolder(View v) {
            super(v);
            this.imgReqIcon = (ImageView) v.findViewById(R.id.exiIcon);
            this.txtReqDescp = (TextView) v.findViewById(R.id.exiDescp);
        }
    }

    /**
     * Set the icon and description for every item in the list.
     * @param iconView The holder view for the icon
     * @param iconSrc The ID for the icon resource
     * @param info The description to show in the toast
     */
    private void setExtraInfoItem(final ImageView iconView, int iconSrc, final String info){
        iconView.setImageResource(iconSrc);
        iconView.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                Toast.makeText(iconView.getContext(),info, Toast.LENGTH_SHORT).show();
                return true;
            }
        });
    }
}
