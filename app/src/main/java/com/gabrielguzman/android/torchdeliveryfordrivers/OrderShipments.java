package com.gabrielguzman.android.torchdeliveryfordrivers;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

/**
 * Manages the shipments list for the selected order.
 * @author Gabriel Guzman
 * @since 2016.21.04
 * @see android.support.v7.app.AppCompatActivity
 */
public class OrderShipments extends AppCompatActivity {

    private View mProgressOverlay;
    private PodUploadTask mPODTask;
    private String uploadMsg = "All PODs uploaded";
    private int orderSts;
    ShipmentAdapter mAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_shipments);

        mProgressOverlay = findViewById(R.id.progress_overlay);
        TextView mProgressMssg = (TextView) findViewById(R.id.progressMssg);
        if (mProgressMssg != null) {
            mProgressMssg.setText(R.string.uploadPodProgressMsg);
        }

        String order = String.format("%s",getIntent().getIntExtra("order",0));
        orderSts = getIntent().getIntExtra("status",0);
        ArrayList<Shipment> shipmentsList= (ArrayList<Shipment>) getIntent().getSerializableExtra("shipments");

        setTitle("Order #"+order);

        Toolbar guidesToolbar = (Toolbar) findViewById(R.id.torchToolbar);
        setSupportActionBar(guidesToolbar);

        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();

        // Enable the Up button
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }

        mAdapter = new ShipmentAdapter(OrderShipments.this, shipmentsList, orderSts);
        ListView shipmentsListView = (ListView) findViewById(R.id.OrderShipmentsLV);
        if (shipmentsListView != null) {
            shipmentsListView.setAdapter(mAdapter);
        }
    }

    //Get photo from the selected app and send it back to the adapter
    private static final int PICK_IMAGE_ID = 1989;
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode) {
            case PICK_IMAGE_ID:
                if (resultCode != 0) {
                    Bitmap bitmap = ImagePicker.getImageFromResult(this, resultCode, data);
                    String img64 = convertToBase64(bitmap);
                    mAdapter.onActivityResult(requestCode, img64);
                }
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }

    /**
     * Method that encodes image to Base64
     * @param image The image to encode
     * @return A base64 string representing the image
     */
    private String convertToBase64(Bitmap image){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] byteArrayImage = baos.toByteArray();
        return Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
    }

    /**
     * Shows a progress dialog and prepares the adapter for the upload
     */
    private void podUpload(){
        DriverOrders.animateView(mProgressOverlay, View.VISIBLE, 1.0f, 50);
        mPODTask = new PodUploadTask();
        mPODTask.execute((Void) null);
    }

    /*-------- Menu methods ---------*/
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (orderSts >= 5)
            getMenuInflater().inflate(R.menu.shipments_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_upload) {
            podUpload();
            return true;
        }
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Custom AsyncTask used to upload the PoDs.
     * @see android.os.AsyncTask
     */
    public class PodUploadTask extends AsyncTask<Void, Void, Boolean> {

        PodUploadTask() { }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                mAdapter.preparePODUpload();
                return true;
            } catch (Exception e) {
                uploadMsg = "Something went wrong, try again later";
            }
            return false;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mPODTask = null;
            DriverOrders.animateView(mProgressOverlay, View.GONE, 0, 200);

            if (success) {
                 Toast.makeText(getBaseContext(),uploadMsg, Toast.LENGTH_SHORT).show();
            } else {
                 Toast.makeText(getBaseContext(),uploadMsg, Toast.LENGTH_LONG).show();
            }
        }

        @Override
        protected void onCancelled() {
            mPODTask = null;
            DriverOrders.animateView(mProgressOverlay, View.GONE, 0, 200);
        }
    }
}