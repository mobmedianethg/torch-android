package com.gabrielguzman.android.torchdeliveryfordrivers;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.ArrayList;

/**
 * Custom adapter for the order's shipments.
 * @author Gabriel Guzman
 * @since 2016.04.05
 * @see android.widget.BaseAdapter
 */
public class ShipmentAdapter extends BaseAdapter{

    private static ArrayList<Shipment> searchArrayList;
    private LayoutInflater mInflater;
    private Context actContext;
    private int orderSts;
    private Shipment shipment, shpm;
    private int pst;

    public ShipmentAdapter(Context context, ArrayList<Shipment> results, int sts) {
        searchArrayList = results;
        actContext = context;
        orderSts = sts;
        mInflater = LayoutInflater.from(context);
    }

    /**
     * Get the amount of items in the list.
     * @return The size of the list
     */
    public int getCount() {
        return searchArrayList.size();
    }

    /**
     * Get a specific item of the list according to the position.
     * @param position The position of the item in the list.
     * @return The item Object.
     */
    public Object getItem(int position) {
        return searchArrayList.get(position);
    }

    /**
     * Get a specific item ID. In this case the position is the ID.<br><br>
     *     This is an auto imported method that needs to be implemented when
     *     you extend {@code BaseAdapter}
     * @param position The position of the item in the list.
     * @return The position.
     */
    public long getItemId(int position) {
        return position;
    }

    /**
     * Method that inflates the view.
     * @param position Position of a specific element in the list.
     * @param convertView The view to convert.
     * @param parent The parent ViewGroup.
     * @return The inflated view.
     * @since 2016.20.04
     */
    public View getView(final int position, View convertView, final ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.shipment_item, parent,false);
            holder = new ViewHolder();
            holder.txtShipmentNumber = (TextView) convertView.findViewById(R.id.shipmentNumberID);
            holder.txtShipmentWeight = (TextView) convertView.findViewById(R.id.shipmentWeight);
            holder.txtShipmentDG = (TextView) convertView.findViewById(R.id.shipmentDG);
            holder.txtShipmentRefrig = (TextView) convertView.findViewById(R.id.shipmentRefrigerated);
            holder.txtShipmentCargoQty = (TextView) convertView.findViewById(R.id.shipmentCargoQty);
            holder.rvExiRow = (RecyclerView) convertView.findViewById(R.id.exiRow);
            holder.rlPODSection = (RelativeLayout) convertView.findViewById(R.id.shipmentPODSection);
            holder.btAddPOD = (ImageView) convertView.findViewById(R.id.podImage);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        shipment = searchArrayList.get(position);

        final String weight = (shipment.getWeight() == null) ? ("Not Provided"):String.format("%s lbs.",shipment.getWeight());
        final String dg = (shipment.getDg()== 1) ? "Yes":"No";
        final String refrig = (shipment.getRefrigerated()== 1) ? "Yes":"No";

        ExtraInfoAdapter adapter = new ExtraInfoAdapter(shipment,actContext);
        holder.rvExiRow.setAdapter(adapter);
        holder.rvExiRow.setLayoutManager(new LinearLayoutManager(parent.getContext(), LinearLayoutManager.HORIZONTAL, false));

        holder.txtShipmentNumber.setText(shipment.getShipmentNumber());
        holder.txtShipmentWeight.setText(weight);
        holder.txtShipmentDG.setText(dg);
        holder.txtShipmentRefrig.setText(refrig);
        holder.txtShipmentCargoQty.setText(String.format("%s", shipment.getShipmentTotalQty()));

        if (orderSts >= 5) {
            if (!shipment.getImgB64().equals(""))
                holder.btAddPOD.setImageBitmap(decodeFromBase64ToBitmap(shipment.getImgB64()));
            else if (!getImgFromDB(shipment.getOrderID(), shipment.getShipmentNumber()).equals("")) {
            shipment.setImgB64(getImgFromDB(shipment.getOrderID(), shipment.getShipmentNumber()));
            searchArrayList.set(position, shipment);
            notifyDataSetChanged();
                holder.btAddPOD.setImageBitmap(decodeFromBase64ToBitmap(getImgFromDB(shipment.getOrderID(), shipment.getShipmentNumber())));
            }

            //Load POD into the shipment item
            holder.btAddPOD.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pst = position;
                    shpm = searchArrayList.get(position);
                    onPickImage();
                }
            });
        }
        else {
            holder.rlPODSection.setVisibility(View.GONE);
        }
        return convertView;
    }

    /**
     * Inner class for the holder to be used in the Shipments ListView.
     * @since 2016.20.04
     */
    static class ViewHolder {
        TextView txtShipmentNumber;
        TextView txtShipmentWeight;
        TextView txtShipmentDG;
        TextView txtShipmentRefrig;
        TextView txtShipmentCargoQty;
        ImageView btAddPOD;
        RecyclerView rvExiRow;
        RelativeLayout rlPODSection;
    }

    //This number represents the requestCode for the startActivityForResult
    private static final int PICK_IMAGE_ID = 1989;

    /**
     * Method that shows a list of apps to select from and pick a photo.
     */
    public void onPickImage() {
        Activity origin = (Activity) actContext;
        Intent chooseImageIntent = ImagePicker.getPickImageIntent(actContext);
        origin.startActivityForResult(chooseImageIntent, PICK_IMAGE_ID);
    }

    /**
     * Gets the encoded image and stores it in the local database.
     * @param requestCode The code that identifies the activity
     * @param img64 Base64 string for the image to store
     * @since 2016.06.05
     */
    public void onActivityResult(int requestCode,String img64) {
        if (requestCode == PICK_IMAGE_ID){

            TorchDBHelper dbHelper = new TorchDBHelper(actContext);

            // Gets the data repository in write mode
            SQLiteDatabase db = dbHelper.getWritableDatabase();

            // Create a new map of values, where column names are the keys
            ContentValues values = new ContentValues();
            values.put(TorchDBContract.OrderShipments.COLUMN_NAME_ORDER_ID,shpm.getOrderID());
            values.put(TorchDBContract.OrderShipments.COLUMN_NAME_SHIPMENT_ID, shpm.getShipmentNumber());
            values.put(TorchDBContract.OrderShipments.COLUMN_NAME_DRIVER_ID, 6);
            values.put(TorchDBContract.OrderShipments.COLUMN_NAME_BASE64_IMAGE, img64);

            // Insert the new row
            db.insert(
                    TorchDBContract.OrderShipments.TABLE_NAME,
                    null,
                    values);
        }

        shpm.setImgB64(img64);
        searchArrayList.set(pst, shpm);
        notifyDataSetChanged();
    }

    /**
     * Decodes a base64 string to a bitmap image
     * @param encodedImage The base64 string to decode
     * @return The bitmap for the image
     * @since 2016.06.05
     */
    private Bitmap decodeFromBase64ToBitmap(String encodedImage){
        byte[] decodedString = Base64.decode(encodedImage, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
    }

    /**
     * Method that search a base64 image from the internal database, using the order ID and
     * the shipment number.
     * @param orderID The ID for the selected order
     * @param shipmentNumber The shown number for the selected shipment
     * @return A stored base64 representation of the image
     * @since 2016.06.05
     */
    private String getImgFromDB(int orderID, String shipmentNumber){

        TorchDBHelper dbHelper = new TorchDBHelper(actContext);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        // Columns to retrieve
        String[] projection = {
                TorchDBContract.OrderShipments._ID,
                TorchDBContract.OrderShipments.COLUMN_NAME_BASE64_IMAGE
        };

        //Where clause
        String selection =
                TorchDBContract.OrderShipments.COLUMN_NAME_ORDER_ID + "= ? AND " +
                TorchDBContract.OrderShipments.COLUMN_NAME_SHIPMENT_ID + "= ?";

        //Arguments for where clause
        String[] selectionArgs = {
                String.format("%s",orderID),
                shipmentNumber
        };

        String sortOrder =
                TorchDBContract.OrderShipments._ID + " DESC";

        Cursor c = db.query(
                TorchDBContract.OrderShipments.TABLE_NAME,// The table to query
                projection,
                selection,
                selectionArgs,
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder
        );

        c.moveToFirst();

        if (c.getCount()>0) {
            String dbImg = c.getString(c.getColumnIndexOrThrow(TorchDBContract.OrderShipments.COLUMN_NAME_BASE64_IMAGE));
            c.close();
            return dbImg;
        }
        else{
            c.close();
            return "";
        }
    }

    /**
     * Method that calls the TorchConnection class to upload the POD image.
     * @param shipmentID The unique ID for the selected shipment
     * @param img64 The base64 string for the image to upload
     * @since 2016.06.05
     */
    private void uploadPOD(int shipmentID, String img64){
        try {
            /*the commented code is used to get the servers response, but apparently having
            return statements in an asynctask that calls this method multiple times cause the thread
            to be finished. This must be addressed. Gabriel Guzman
             */
            //JSONObject uploadResponse = TorchConnection.uploadPOD(shipmentID,img64);
            TorchConnection.uploadPOD(shipmentID,img64);
           /* Log.e("upResp",uploadResponse+"");
            if (uploadResponse.has("status")) {
                if(uploadResponse.getString("status").equals("success")){
                    Log.e("uploadPOD",uploadResponse.getString("message"));
                }
                else
                    Log.e("uploadPOD",uploadResponse.getString("status"));
            }*/
        }
        catch (Exception e){
            //Log.e("errorUploadPOD", e.getMessage());
        }
    }

    /**
     * Method that cycles through the list of shipments and initializes the upload for those that
     * have an image.
     * @since 2016.06.05
     */
    public void preparePODUpload(){
        for (int i = 0; i < searchArrayList.size(); i++){
            //Log.e("preparePODUpload", i + " arraySize: " + searchArrayList.size());
            if (!getImgFromDB(searchArrayList.get(i).getOrderID(), searchArrayList.get(i).getShipmentNumber()).equals(""))
            //if (!searchArrayList.get(i).getImgB64().equals(""))
                uploadPOD(searchArrayList.get(i).getShipmentID(),getImgFromDB(searchArrayList.get(i).getOrderID(), searchArrayList.get(i).getShipmentNumber()));
        }
    }
}