package com.gabrielguzman.android.torchdeliveryfordrivers;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

/** Class for the About screen.
 * @author Gabriel Guzman
 * @since 2016.20.04
 * @see android.support.v7.app.AppCompatActivity
 */
public class About extends AppCompatActivity{

    private View mDummy;
    private int mDummyClick=0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        Toolbar guidesToolbar = (Toolbar) findViewById(R.id.torchAboutToolbar);
        setSupportActionBar(guidesToolbar);

        ActionBar ab = getSupportActionBar();

        // Enable the Up button
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }

        View mLogo = findViewById(R.id.torchLogoAbout);

        if (mLogo != null) {
            mLogo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDummyClick++;
                    if (mDummyClick >6) {
                        mDummy = findViewById(R.id.dummy);
                        if (mDummy != null) {
                            mDummy.setVisibility(View.VISIBLE);
                        }
                    }
                }
            });
        }
    }
}
