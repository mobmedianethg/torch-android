package com.gabrielguzman.android.torchdeliveryfordrivers;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;

/**
 * Manages the orders list for the driver using the app.
 * @author Gabriel Guzman
 * @since 2016.18.04
 * @see android.support.v7.app.AppCompatActivity
 */
public class DriverOrders extends AppCompatActivity {

    // Keep track of the get orders task to ensure it can be cancelled if requested.
    private DriverOrdersTask mOrdersTask = null;
    private View mProgressOverlay;
    private ArrayList<Order> ordersList;
    private Boolean loadOrders = true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_orders);

        // Get the shared preferences for driver's preferences
        SharedPreferences driversPref =  getSharedPreferences("drivers_pref", MODE_PRIVATE);

        //Set the toolbar
        Toolbar torchToolbar = (Toolbar) findViewById(R.id.torchToolbar);
        setSupportActionBar(torchToolbar);
        ActionBar sab = getSupportActionBar();
        if (sab != null)
            sab.setSubtitle("For "+driversPref.getString("driverName","current driver"));

        if (loadOrders)
            fetchOrders(driversPref.getInt("driverID",0));
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        // Get the shared preferences for driver's preferences
        SharedPreferences driversPref =  getSharedPreferences("drivers_pref", MODE_PRIVATE);
        fetchOrders(driversPref.getInt("driverID",0));
    }

    private void fetchOrders(int driverID){
        mProgressOverlay = findViewById(R.id.progress_overlay);
        TextView mProgressMssg = (TextView) findViewById(R.id.progressMssg);
        if (mProgressMssg != null) {
            mProgressMssg.setText(R.string.checkingOrdersProgressMsg);
        }

        animateView(mProgressOverlay, View.VISIBLE, 1.0f, 50);
        mOrdersTask = new DriverOrdersTask(driverID);
        mOrdersTask.execute((Void) null);
    }

    public class DriverOrdersTask extends AsyncTask<Void, Void, Boolean> {

        private final int mDriverID;
        private String mErrorMsg="There was a problem fetching orders.";

        DriverOrdersTask(int driverID) {
            mDriverID = driverID;
        }

        @Override
        protected Boolean doInBackground(Void... params) {

             ordersList = new ArrayList<>();

            try {
                JSONObject ordersResponse = TorchConnection.requestDriverOrders(mDriverID);

                if (ordersResponse.has("status")){
                    if (ordersResponse.getString("status").equals("success")) {

                        JSONArray ordersArray = ordersResponse.getJSONArray("orders");

                        try {

                            if (ordersArray != null) {
                                for (int i = 0; i < ordersArray.length(); i++) {
                                    JSONObject jsonOrder = ordersArray.getJSONObject(i);
                                    Order objOrder = new Order();
                                    Boolean missingPod = false;

                                    objOrder.setNumOrden(jsonOrder.getInt("id"));
                                    objOrder.setStatus(jsonOrder.getInt("status"));
                                    objOrder.setDriverID(jsonOrder.getInt("driver_id"));
                                    if (!jsonOrder.isNull("vehicle_id"))
                                        objOrder.setVehicleID(jsonOrder.getInt("vehicle_id"));
                                    if (!jsonOrder.isNull("viaje_id"))
                                        objOrder.setViajeID(jsonOrder.getInt("viaje_id"));
                                    objOrder.setFrom(jsonOrder.getString("from") + "");
                                    objOrder.setTo(jsonOrder.getString("to") + "");
                                    objOrder.setAddressFrom(jsonOrder.getString("addressfrom"));
                                    objOrder.setAddressTo(jsonOrder.getString("addressto"));
                                    objOrder.setCreatedAt(jsonOrder.getString("created_at"));
                                    if (!jsonOrder.isNull("totalWeight"))
                                        objOrder.setTotalWeight(jsonOrder.getInt("totalWeight"));
                                    if (!jsonOrder.isNull("totalQuantity"))
                                        objOrder.setTotalQuantity(jsonOrder.getInt("totalQuantity"));
                                    objOrder.setFecha(jsonOrder.getString("fecha"));

                                    ArrayList<Shipment> orderShipments= new ArrayList<>();

                                try {
                                    JSONArray shipmentsArray = jsonOrder.getJSONArray("shipments");

                                    if (shipmentsArray != null) {
                                        for (int j = 0; j < shipmentsArray.length(); j++) {
                                            JSONObject jsonShipment = shipmentsArray.getJSONObject(j);
                                            Shipment objShip = new Shipment();

                                            objShip.setShipmentID(jsonShipment.getInt("id"));
                                            objShip.setOrderID(jsonShipment.getInt("order_id"));
                                            objShip.setShipmentNumber(jsonShipment.getString("shipment_number"));
                                            if(jsonShipment.isNull("pod"))
                                                missingPod = true;
                                            if(!jsonShipment.isNull("dg"))
                                                objShip.setDg(jsonShipment.getInt("dg"));
                                            if(!jsonShipment.isNull("refrigerated"))
                                                objShip.setRefrigerated(jsonShipment.getInt("refrigerated"));
                                            if(!jsonShipment.isNull("weight"))
                                                objShip.setWeight(jsonShipment.getString("weight"));
                                            if (!jsonShipment.isNull("total_quantity"))
                                                objShip.setShipmentTotalQty(jsonShipment.getInt("total_quantity"));
                                            if(!jsonShipment.isNull("garbage_disposal"))
                                                objShip.setGarbageDisposal(jsonShipment.getInt("garbage_disposal"));
                                            if(!jsonShipment.isNull("flatbed"))
                                                objShip.setFlatbed(jsonShipment.getInt("flatbed"));
                                            if(!jsonShipment.isNull("air_ride"))
                                                objShip.setAirRide(jsonShipment.getInt("air_ride"));
                                            if(!jsonShipment.isNull("roller_bed"))
                                                objShip.setRollerBed(jsonShipment.getInt("roller_bed"));
                                            if(!jsonShipment.isNull("extra_person"))
                                                objShip.setExtraPerson(jsonShipment.getInt("extra_person"));
                                            if(!jsonShipment.isNull("liftgate"))
                                                objShip.setLiftGate(jsonShipment.getInt("liftgate"));
                                            if(!jsonShipment.isNull("inside"))
                                                objShip.setInside(jsonShipment.getInt("inside"));
                                            if(!jsonShipment.isNull("breakdown"))
                                                objShip.setBreakdown(jsonShipment.getInt("breakdown"));
                                            if(!jsonShipment.isNull("residential"))
                                                objShip.setResidential(jsonShipment.getInt("residential"));
                                            objShip.setOther(jsonShipment.getString("other"));

                                            orderShipments.add(objShip);
                                        }
                                    }
                                }
                                catch (Exception e){/*Log.e("for de shipments", e.getMessage());*/}

                                    objOrder.setShipments(orderShipments);

                                    /*If an order was delivered and all the PoDs have been uploaded, skip it.
                                      If it was delivered but a Pod is missing, activate the flag to show a warning
                                     */
                                    if (jsonOrder.getInt("status")== 6 && !missingPod)
                                        continue;
                                    else if (jsonOrder.getInt("status")== 6 && missingPod)
                                        objOrder.setMissingPod(true);

                                    ordersList.add(objOrder);

                                }
                                return true;
                            }
                        }catch (Exception e) {/*Log.e("for de ordenes", e.getMessage());*/}

                    }
                    else if (ordersResponse.getString("status").equals("error") &&
                             ordersResponse.getString("alert").equals("No orders asigned")){
                        mErrorMsg = "No orders assigned.";
                    }
                }
            } catch (Exception e) {/*Log.e("order request",(e.getMessage()==null)? "Order Request error":e.getMessage());*/}
            return false;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mOrdersTask = null;
            animateView(mProgressOverlay, View.GONE, 0, 200);

            ListView ordersListView = (ListView) findViewById(R.id.orders);

            if (ordersListView != null) {
                if (success) {
                    ordersListView.setAdapter(new OrderSimpleAdapter(DriverOrders.this, ordersList, mDriverID));
                    loadOrders = false;
                }
                else{
                    Toast.makeText(getBaseContext(),mErrorMsg, Toast.LENGTH_SHORT).show();
                }
                ordersListView.setEmptyView(findViewById(R.id.emptyOrdersView));
            }
        }

        @Override
        protected void onCancelled() {
            mOrdersTask = null;
            animateView(mProgressOverlay, View.GONE, 0, 200);
        }
    }

    /**
     * @param view         View to animate
     * @param toVisibility Visibility at the end of animation
     * @param toAlpha      Alpha at the end of animation
     * @param duration     Animation duration in ms
     */
    public static void animateView(final View view, final int toVisibility, float toAlpha, int duration) {
        boolean show = toVisibility == View.VISIBLE;
        if (show) {
            view.setAlpha(0);
        }
        view.setVisibility(View.VISIBLE);
        view.animate()
                .setDuration(duration)
                .alpha(show ? toAlpha : 0)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.setVisibility(toVisibility);
                    }
                });
    }

    /*-------- Menu methods ---------*/
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_about) {
            Intent aboutIntent = new Intent(this,About.class);
            startActivity(aboutIntent);
            return true;
        }
        else if (id == R.id.action_refresh){
            SharedPreferences driversPref =  getSharedPreferences("drivers_pref", MODE_PRIVATE);
            fetchOrders(driversPref.getInt("driverID",0));
        }
        else if (id == R.id.action_logout ){
            SharedPreferences driversPref =  getSharedPreferences("drivers_pref", MODE_PRIVATE);
            driversPref.edit().putBoolean("registered",false);
            driversPref.edit().clear().apply();
            Intent loginIntent = new Intent(this,LoginActivity.class);
            startActivity(loginIntent);
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}