package com.gabrielguzman.android.torchdeliveryfordrivers;

import android.provider.BaseColumns;

/**
 * Contract class for the tables to use in the database.
 * @author Gabriel Guzman
 * @since 2016.06.05
 */
public final class TorchDBContract {

    // To prevent someone from accidentally instantiating the contract class, empty constructor
    public TorchDBContract() {}

    /**
     *  Inner class that defines the table contents. Implements {@code BaseColumns}
     *  <br>
     *      for the _ID column.
     * @see android.provider.BaseColumns      *
     */
    public static abstract class OrderShipments implements BaseColumns {
        public static final String TABLE_NAME = "shipments";
        public static final String COLUMN_NAME_ORDER_ID = "orderid";
        public static final String COLUMN_NAME_SHIPMENT_ID = "shipmentid";
        public static final String COLUMN_NAME_DRIVER_ID = "driverid";
        public static final String COLUMN_NAME_BASE64_IMAGE = "basesfimage";
    }
}