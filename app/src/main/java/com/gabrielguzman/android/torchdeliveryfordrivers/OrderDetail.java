package com.gabrielguzman.android.torchdeliveryfordrivers;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ListView;
import java.util.ArrayList;

/**
 * Shows the details of the selected order.
 * @author Gabriel Guzman
 * @since 2016.11.06
 * @see AppCompatActivity
 */
public class OrderDetail extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);

        // Get the shared preferences for driver's preferences
        SharedPreferences driversPref =  getSharedPreferences("drivers_pref", MODE_PRIVATE);

        //Set the toolbar
        Toolbar torchToolbar = (Toolbar) findViewById(R.id.torchToolbar);
        setSupportActionBar(torchToolbar);

        // Enable the Up button
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        ListView ordersListView = (ListView) findViewById(R.id.orders);
        ArrayList<Order> ordersList = (ArrayList<Order>) getIntent().getSerializableExtra("orderInfo");
        setTitle(String.format("Order #%s", ordersList.get(0).getNumOrden()));
        if (ordersListView != null) {
            ordersListView.setAdapter(new OrderAdapter(this, ordersList, driversPref.getInt("driverID",0)));
            ordersListView.setEmptyView(findViewById(R.id.emptyOrderDetailView));
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //Simulate back button
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}