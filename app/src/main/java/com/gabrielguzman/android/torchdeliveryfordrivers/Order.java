package com.gabrielguzman.android.torchdeliveryfordrivers;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Class that defines an order and its elements.
 * @author Gabriel Guzman
 * @since 2016.20.04
 */
public class Order implements Serializable{

    private int status = 0;
    private int viajeID = 0;
    private int vehicleID = 0;
    private int driverID = 0;
    private String contact =""; //check DB type
    private String from ="";
    private String to ="";
    private String addressFrom ="";
    private String addressTo ="";
    private String createdAt ="";
    private int numOrden = 0;
    private int totalWeight = 0;
    private int totalQuantity = 0;
    private String fecha ="";
    //Extra info
    private Boolean rush = false;
    private Boolean liftGate = false;
    private Boolean extraPerson = false;
    private Boolean inside = false;
    private Boolean breakdown = false;
    private Boolean residential = false;
    private ArrayList<Shipment> shipments;
    //Internal field to keep track of PoDs
    private Boolean missingPod = false;

    public Boolean getMissingPod() {
        return missingPod;
    }

    public void setMissingPod(Boolean missingPod) {
        this.missingPod = missingPod;
    }

    public ArrayList<Shipment> getShipments() {
        return shipments;
    }

    public void setShipments(ArrayList<Shipment> shipments) {
        this.shipments = shipments;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getViajeID() {
        return viajeID;
    }

    public void setViajeID(int viajeID) {
        this.viajeID = viajeID;
    }

    public int getVehicleID() {
        return vehicleID;
    }

    public void setVehicleID(int vehicleID) {
        this.vehicleID = vehicleID;
    }

    public int getDriverID() {
        return driverID;
    }

    public void setDriverID(int driverID) {
        this.driverID = driverID;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getAddressFrom() {
        return addressFrom;
    }

    public void setAddressFrom(String addressFrom) {
        this.addressFrom = addressFrom;
    }

    public String getAddressTo() {
        return addressTo;
    }

    public void setAddressTo(String addressTo) {
        this.addressTo = addressTo;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public int getNumOrden() {
        return numOrden;
    }

    public void setNumOrden(int numOrden) {
        this.numOrden = numOrden;
    }

    public int getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(int totalWeight) {
        this.totalWeight = totalWeight;
    }

    public int getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(int totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Boolean getRush() {
        return rush;
    }

    public void setRush(Boolean rush) {
        this.rush = rush;
    }

    public Boolean getLiftGate() {
        return liftGate;
    }

    public void setLiftGate(Boolean liftGate) {
        this.liftGate = liftGate;
    }

    public Boolean getExtraPerson() {
        return extraPerson;
    }

    public void setExtraPerson(Boolean extraPerson) {
        this.extraPerson = extraPerson;
    }

    public Boolean getInside() {
        return inside;
    }

    public void setInside(Boolean inside) {
        this.inside = inside;
    }

    public Boolean getBreakdown() {
        return breakdown;
    }

    public void setBreakdown(Boolean breakdown) {
        this.breakdown = breakdown;
    }

    public Boolean getResidential() {
        return residential;
    }

    public void setResidential(Boolean residential) {
        this.residential = residential;
    }
}