package com.gabrielguzman.android.torchdeliveryfordrivers;

import java.io.Serializable;

/**
 * Class that defines a shipment and its elements.
 * @author Gabriel Guzman
 * @since 2016.04.05
 */
public class Shipment implements Serializable{

    private int shipmentID = 0;
    private int orderID = 0;
    private String shipmentNumber ="";
    private int dg = 0;
    private int refrigerated = 0;
    private int shipmentTotalQty = 0;
    private String weight ="Not provided";
    private String createdAt ="";
    private String updatedAt ="";
    //Extra info
    private int garbageDisposal = 0;
    private int flatbed = 0;
    private int liftGate = 0;
    private int extraPerson = 0;
    private int inside = 0;
    private int breakdown = 0;
    private int residential = 0;
    private int airRide = 0;
    private int rollerBed = 0;
    private String other ="";
    private String imgB64 = "";

    public String getImgB64() {
        return imgB64;
    }

    public void setImgB64(String imgB64) {
        this.imgB64 = imgB64;
    }

    public int getShipmentID() {
        return shipmentID;
    }

    public void setShipmentID(int shipmentID) {
        this.shipmentID = shipmentID;
    }

    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public String getShipmentNumber() {
        return shipmentNumber;
    }

    public void setShipmentNumber(String shipmentNumber) {
        this.shipmentNumber = shipmentNumber;
    }

    public int getDg() {
        return dg;
    }

    public void setDg(int dg) {
        this.dg = dg;
    }

    public int getRefrigerated() {
        return refrigerated;
    }

    public void setRefrigerated(int refrigerated) {
        this.refrigerated = refrigerated;
    }

    public int getShipmentTotalQty() {
        return shipmentTotalQty;
    }

    public void setShipmentTotalQty(int shipmentTotalQty) {
        this.shipmentTotalQty = shipmentTotalQty;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getGarbageDisposal() {
        return garbageDisposal;
    }

    public void setGarbageDisposal(int garbageDisposal) {
        this.garbageDisposal = garbageDisposal;
    }

    public int getFlatbed() {
        return flatbed;
    }

    public void setFlatbed(int flatbed) {
        this.flatbed = flatbed;
    }

    public int getLiftGate() {
        return liftGate;
    }

    public void setLiftGate(int liftGate) {
        this.liftGate = liftGate;
    }

    public int getExtraPerson() {
        return extraPerson;
    }

    public void setExtraPerson(int extraPerson) {
        this.extraPerson = extraPerson;
    }

    public int getInside() {
        return inside;
    }

    public void setInside(int inside) {
        this.inside = inside;
    }

    public int getBreakdown() {
        return breakdown;
    }

    public void setBreakdown(int breakdown) {
        this.breakdown = breakdown;
    }

    public int getResidential() {
        return residential;
    }

    public void setResidential(int residential) {
        this.residential = residential;
    }

    public int getAirRide() {
        return airRide;
    }

    public void setAirRide(int airRide) {
        this.airRide = airRide;
    }

    public int getRollerBed() {
        return rollerBed;
    }

    public void setRollerBed(int rollerBed) {
        this.rollerBed = rollerBed;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }
}