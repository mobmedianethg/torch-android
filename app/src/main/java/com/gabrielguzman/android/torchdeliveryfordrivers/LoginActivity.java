package com.gabrielguzman.android.torchdeliveryfordrivers;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import org.json.JSONObject;

/**
 * A login screen that offers access via username/password.
 * @author Gabriel Guzman
 * @since 2016.18.04
 * @see android.support.v7.app.AppCompatActivity
 */
public class LoginActivity extends AppCompatActivity {

    // Keep track of the login task to ensure we can cancel it if requested.
    private UserLoginTask mAuthTask = null;

    // UI references.
    private EditText mUsernameView;
    private EditText mPasswordView;
    private View mProgressOverlay;
    private Button mEmailSignInButton;
    private SharedPreferences driversPref;
    private String errorMsg = "Invalid credentials";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Get the driver shared preferences
        driversPref =  getSharedPreferences("drivers_pref", MODE_PRIVATE);

        // Go to Orders if the user is already logged
        if(driversPref.getBoolean("registered",false)) {
            Intent homeIntent = new Intent(this,DriverOrders.class);
            startActivity(homeIntent);
            finish();
        }

        setContentView(R.layout.activity_login);

        mUsernameView = (EditText) findViewById(R.id.user);
        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        mEmailSignInButton = (Button) findViewById(R.id.sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mProgressOverlay = findViewById(R.id.progress_overlay);
        ImageView mLogo = (ImageView) findViewById(R.id.torchLogoLogin);
        if (mLogo != null) {
            mLogo.requestFocus();
        }
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid user, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mUsernameView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String user = mUsernameView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid username.
        if (TextUtils.isEmpty(user)) {
            mUsernameView.setError(getString(R.string.error_field_required));
            focusView = mUsernameView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            mEmailSignInButton.setEnabled(false);

            animateView(mProgressOverlay, View.VISIBLE, 1.0f, 200);

            mAuthTask = new UserLoginTask(user, password);
            mAuthTask.execute((Void) null);
        }
    }

    /**
     * Asynchronous login task used to authenticate the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private final String mEmail;
        private final String mPassword;

        UserLoginTask(String user, String password) {
            mEmail = user;
            mPassword = password;
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            try {
                JSONObject loginResponse = TorchConnection.requestDriverLogin(mEmail,mPassword);

                if (loginResponse.has("status")){
                    if (loginResponse.getString("status").equals("success")) {
                        driversPref.edit().putBoolean("registered",true).apply();
                        driversPref.edit().putInt("driverID",loginResponse.getInt("driver_id")).apply();
                        driversPref.edit().putString("driverName", loginResponse.getString("driver_name")).apply();
                        return true;
                    }
                }
            } catch (Exception e) {
                errorMsg = "Login failed";
            }
            return false;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            animateView(mProgressOverlay, View.GONE, 0, 200);

            if (success) {
                Intent homeIntent = new Intent(getBaseContext(),DriverOrders.class);
                finish();
                startActivity(homeIntent);
            } else {
                mEmailSignInButton.setEnabled(true);
                Toast.makeText(getBaseContext(),errorMsg, Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            animateView(mProgressOverlay, View.GONE, 0, 200);
            mEmailSignInButton.setEnabled(true);
        }
    }

    /**
     * @param view         View to animate
     * @param toVisibility Visibility at the end of animation
     * @param toAlpha      Alpha at the end of animation
     * @param duration     Animation duration in ms
     */
    public static void animateView(final View view, final int toVisibility, float toAlpha, int duration) {
        boolean show = toVisibility == View.VISIBLE;
        if (show) {
            view.setAlpha(0);
        }
        view.setVisibility(View.VISIBLE);
        view.animate()
                .setDuration(duration)
                .alpha(show ? toAlpha : 0)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.setVisibility(toVisibility);
                    }
                });
    }
}

