package com.gabrielguzman.android.torchdeliveryfordrivers;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Simpler custom adapter for the driver's orders, to be used in the initial list. This was added
 * later at development. Because of time constraints the orders views are managed at two different
 * activities {@code DriverOrders} and {@code OrderDetail}, the optimal way would be using two
 * fragments.
 * @author Gabriel Guzman
 * @since 2016.11.06
 * @see BaseAdapter
 */
public class OrderSimpleAdapter extends BaseAdapter{

    private static ArrayList<Order> searchArrayList;
    private ArrayList<Order> selectedOrder;
    private LayoutInflater mInflater;
    private int mDriverID;
    private Context mContext;

    public OrderSimpleAdapter(Context context, ArrayList<Order> results, int driverID) {
        searchArrayList = results;
        mDriverID = driverID;
        mContext = context;
        mInflater = LayoutInflater.from(context);
    }

    /**
     * Get the amount of items in the list.
     * @return The size of the list
     */
    public int getCount() {
        return searchArrayList.size();
    }

    /**
     * Get a specific item of the list according to the position.
     * @param position The position of the item in the list.
     * @return The item Object.
     */
    public Object getItem(int position) {
        return searchArrayList.get(position);
    }

    /**
     * Get a specific item ID. In this case the position is the ID.<br><br>
     *     This is an auto imported method that needs to be implemented when
     *     you extend {@code BaseAdapter}
     * @param position The position of the item in the list.
     * @return The position.
     */
    public long getItemId(int position) {
        return position;
    }

    /**
     * Method that inflates the view.
     * @param position Position of a specific element in the list.
     * @param convertView The view to convert.
     * @param parent The parent ViewGroup.
     * @return The inflated view.
     * @since 2016.20.04
     */
    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.order_simple_item, parent,false);
            holder = new ViewHolder();
            holder.txtOrderTitle = (TextView) convertView.findViewById(R.id.simpleOrderNumber);
            holder.txtPodWarning  = (TextView) convertView.findViewById(R.id.simpleOrderPODWarning);
            holder.imOrderStsIcon = (ImageView) convertView.findViewById(R.id.simpleOrderStatusImg);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final Order order = searchArrayList.get(position);
        final int numOrden = order.getNumOrden();
        final int orderStatus = order.getStatus();
        final String stsDescpArray[] = {"Error", "Pending", "Left for Origin", "Pickup",
                                  "In Transit", "Arrived", "Delivered"};
        final int stsImgArray[] = {R.mipmap.sts_pending, R.mipmap.sts_pending,R.mipmap.sts_left_origin,R.mipmap.sts_pick_up,
                R.mipmap.sts_in_transit, R.mipmap.sts_arrived, R.mipmap.sts_delivered};

        if (order.getMissingPod())
            holder.txtPodWarning.setVisibility(View.VISIBLE);
        else
            holder.txtPodWarning.setVisibility(View.GONE);

        holder.txtOrderTitle.setText(String.format("#%s - Status: %s",numOrden, stsDescpArray[orderStatus]));
        holder.imOrderStsIcon.setImageResource(stsImgArray[orderStatus]);

        /*This is made this way to save time, because this adapter was added later for a client's
        requirement. An ArrayList with only one element is passed to the OrderDetail class.*/
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedOrder = new ArrayList<>();
                selectedOrder.add(searchArrayList.get(position));
                Intent order = new Intent(parent.getContext(),OrderDetail.class);
                order.putExtra("orderInfo", (Serializable) selectedOrder);
                parent.getContext().startActivity(order);
            }
        });
        return convertView;
    }

    /**
     * Inner class for the holder to be used in the Orders ListView.
     * @since 2016.20.04
     */
    static class ViewHolder {
        TextView txtOrderTitle;
        TextView txtPodWarning;
        ImageView imOrderStsIcon;
    }
}