package com.gabrielguzman.android.torchdeliveryfordrivers;

import android.app.Application;
import com.flurry.android.FlurryAgent;

/**
 * Class that extends {@code Application} for initializing the Flurry Analytics
 * @author Gabriel Guzman
 * @since 2016.13.06
 * @see android.app.Application
 */
public class TorchDeliveryForDrivers extends Application {
    private final static String FLURRY_API_KEY = "7C9XFF6VFJGRVGX5JTWG";
    @Override
    public void onCreate() {
        super.onCreate();

        new FlurryAgent.Builder()
                .withLogEnabled(false)
                .build(this, FLURRY_API_KEY);
    }
}