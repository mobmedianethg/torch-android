package com.gabrielguzman.android.torchdeliveryfordrivers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Class that manages the internal database.
 * @author Gabriel Guzman
 * @since 2016.06.05
 * @see android.database.sqlite.SQLiteOpenHelper
 */
public class TorchDBHelper extends SQLiteOpenHelper {

    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Torch.db";

    public TorchDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    private static final String TEXT_TYPE = " TEXT";
    private static final String INT_TYPE = " INTEGER";
    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + TorchDBContract.OrderShipments.TABLE_NAME + " (" +
                    TorchDBContract.OrderShipments._ID + " INTEGER PRIMARY KEY," +
                    TorchDBContract.OrderShipments.COLUMN_NAME_ORDER_ID + INT_TYPE  + COMMA_SEP +
                    TorchDBContract.OrderShipments.COLUMN_NAME_SHIPMENT_ID + INT_TYPE + COMMA_SEP +
                    TorchDBContract.OrderShipments.COLUMN_NAME_DRIVER_ID + INT_TYPE  + COMMA_SEP +
                    TorchDBContract.OrderShipments.COLUMN_NAME_BASE64_IMAGE + TEXT_TYPE +
                    " )";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + TorchDBContract.OrderShipments.TABLE_NAME;
}