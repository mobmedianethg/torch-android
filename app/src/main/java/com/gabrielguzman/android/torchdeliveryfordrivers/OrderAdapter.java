package com.gabrielguzman.android.torchdeliveryfordrivers;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import org.json.JSONObject;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Custom adapter for the selected order.
 * @author Gabriel Guzman
 * @since 2016.20.04
 * @see android.widget.BaseAdapter
 */
public class OrderAdapter extends BaseAdapter{

    private static ArrayList<Order> searchArrayList;
    private LayoutInflater mInflater;
    private int mDriverID;
    private Context mContext;

    public OrderAdapter(Context context, ArrayList<Order> results, int driverID) {
        searchArrayList = results;
        mDriverID = driverID;
        mContext = context;
        mInflater = LayoutInflater.from(context);
    }

    /**
     * Get the amount of items in the list.
     * @return The size of the list
     */
    public int getCount() {
        return searchArrayList.size();
    }

    /**
     * Get a specific item of the list according to the position.
     * @param position The position of the item in the list.
     * @return The item Object.
     */
    public Object getItem(int position) {
        return searchArrayList.get(position);
    }

    /**
     * Get a specific item ID. In this case the position is the ID.<br><br>
     *     This is an auto imported method that needs to be implemented when
     *     you extend {@code BaseAdapter}
     * @param position The position of the item in the list.
     * @return The position.
     */
    public long getItemId(int position) {
        return position;
    }

    /**
     * Method that inflates the view.
     * @param position Position of a specific element in the list.
     * @param convertView The view to convert.
     * @param parent The parent ViewGroup.
     * @return The inflated view.
     * @since 2016.20.04
     */
    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.order_item, parent,false);
            holder = new ViewHolder();
            holder.txtOrderNumber = (TextView) convertView.findViewById(R.id.orderNumberID);
            holder.txtAddress = (TextView) convertView.findViewById(R.id.orderAddressFrom);
            holder.txtShipmentQty = (TextView) convertView.findViewById(R.id.orderShipmentsQty);
            holder.txtTotalWeight = (TextView) convertView.findViewById(R.id.orderTotalWeight);
            holder.txtOrderStsDescp = (TextView) convertView.findViewById(R.id.orderStatusDescp);
            holder.txtPodWarning = (TextView) convertView.findViewById(R.id.orderPODWarning);
            holder.imOrderStsIcon = (ImageView) convertView.findViewById(R.id.orderStatusImg);
            holder.btShipments = (Button) convertView.findViewById(R.id.shipmentDetailBt);
            holder.btChangeSts = (Button) convertView.findViewById(R.id.updateStsBt);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final Order order = searchArrayList.get(position);
        final int numOrden = order.getNumOrden();
        final int orderStatus = order.getStatus();
        final String totalQty = (order.getTotalQuantity()==0) ? ("Not Provided"):String.format("%s",order.getTotalQuantity());
        final String totalWeight = (order.getTotalWeight()==0) ? ("Not Provided"):String.format("%s lbs.",order.getTotalWeight()) ;

        final int stsImgArray[] = {R.mipmap.sts_pending, R.mipmap.sts_pending,R.mipmap.sts_left_origin,R.mipmap.sts_pick_up,
                             R.mipmap.sts_in_transit, R.mipmap.sts_arrived, R.mipmap.sts_delivered};
        final String stsDescpArray[] = {"Error", "Pending", "Left for\nOrigin", "Pickup",
                                  "In Transit", "Arrived", "Delivered"};

        holder.txtOrderNumber.setText(String.format("%s",numOrden));
        holder.txtAddress.setText(String.format(mContext.getResources().getString(R.string.orderAddress),
                order.getFrom(), order.getAddressFrom(),System.getProperty("line.separator"),
                System.getProperty("line.separator"), order.getTo(), order.getAddressTo()));

        holder.txtShipmentQty.setText(totalQty);
        holder.txtTotalWeight.setText(totalWeight);
        holder.txtOrderStsDescp.setText(stsDescpArray[orderStatus]);

        if (order.getMissingPod())
            holder.txtPodWarning.setVisibility(View.VISIBLE);
        else
            holder.txtPodWarning.setVisibility(View.GONE);

        holder.imOrderStsIcon.setImageResource(stsImgArray[orderStatus]);

        holder.btShipments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent shipments = new Intent(parent.getContext(),OrderShipments.class);
                shipments.putExtra("order", numOrden);
                shipments.putExtra("status", orderStatus);
                shipments.putExtra("shipments", (Serializable) order.getShipments());
                parent.getContext().startActivity(shipments);
            }
        });

        //Snackbar's callback that allows the user to undo an order status change
        final Snackbar.Callback snCallback = new Snackbar.Callback() {

            @Override
            public void onDismissed(Snackbar snackbar, int event) {
                super.onDismissed(snackbar, event);

                //Only update if the snackbar is dismissed by a swype or after timeout
                if ((event == DISMISS_EVENT_SWIPE) || (event == DISMISS_EVENT_TIMEOUT)){
                    updateStatus(mDriverID, numOrden, orderStatus + 1);
                }

                holder.btShipments.setEnabled(true);
                holder.btShipments.setBackgroundColor(ContextCompat.getColor(mContext,
                        android.R.color.holo_blue_light));
            }

        };

        if (orderStatus < 6) {
            holder.btChangeSts.setText(String.format(mContext.getResources().getString(R.string.chgOrderStsBtText),
                    stsDescpArray[orderStatus + 1]));

            holder.btChangeSts.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    order.setStatus(orderStatus+1);
                    searchArrayList.set(position, order);
                    holder.btShipments.setEnabled(false);
                    holder.btShipments.setBackgroundColor(Color.GRAY);
                    notifyDataSetChanged();

                    //Snackbar that shows a button to undo the order status change
                    Snackbar.make(parent, "Order will be updated", Snackbar.LENGTH_LONG)
                            .setAction("Undo", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    order.setStatus(orderStatus);
                                    searchArrayList.set(position, order);
                                    holder.btShipments.setEnabled(true);
                                    holder.btShipments.setBackgroundColor(ContextCompat.getColor(mContext,
                                            android.R.color.holo_blue_light));
                                    notifyDataSetChanged();
                                }
                            })
                            .setActionTextColor(ContextCompat.getColor(mContext,
                                    R.color.colorAccent))
                            .setCallback(snCallback)
                            .show();
                }
            });
        }
        else{
            holder.btChangeSts.setText(R.string.delivOrderStsBtText);
            holder.btChangeSts.setClickable(false);
        }
        return convertView;
    }

    /**
     * Inner class for the holder to be used in the Orders ListView.
     * @since 2016.20.04
     */
    static class ViewHolder {
        TextView txtOrderNumber;
        TextView txtAddress;
        TextView txtShipmentQty;
        TextView txtTotalWeight;
        TextView txtOrderStsDescp;
        TextView txtPodWarning;
        ImageView imOrderStsIcon;
        Button btShipments;
        Button btChangeSts;
    }

    private void updateStatus (int driverID, int orderID, int orderSts){
        try {
            JSONObject updateResponse = TorchConnection.updateOrderStatus(driverID, orderID, orderSts);

            if (updateResponse.has("status")) {
                if (updateResponse.getString("status").equals("success")) {
                    //Toast added here just in case, doesn't show
                    Toast.makeText(mContext, "Order Updated", Toast.LENGTH_LONG).show();
                    //Log.i("update","Order status updated");
                }
            }
        }
        catch (Exception e){
            //Log.e("updateStatus", (e.getMessage()==null)? "There was a connection problem, please try again":e.getMessage());
            Toast.makeText(mContext, "There was a connection problem, please try again", Toast.LENGTH_LONG).show();
        }
    }
}