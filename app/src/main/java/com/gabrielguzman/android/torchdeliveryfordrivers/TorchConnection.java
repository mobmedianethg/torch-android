package com.gabrielguzman.android.torchdeliveryfordrivers;

import android.os.Build;
import android.os.StrictMode;
import android.util.Log;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedInputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

/**
 * Class that manages all the connections and requests to the
 * Torch backend.
 * @author Gabriel Guzman
 * @version 1.0
 * @since 2016.26.04
 */
public class TorchConnection {

    //URLs for the Torch backend
    //***DEVELOPMENT & TEST***
    private static final String SERVERURL = "http://torchdev.mobilemediacms.com/api/v1/drivers_data/";
    //***PRODUCTION***
    //private static final String SERVERURL = "";

    private static final String DRIVERLOGINURL = SERVERURL + "login";
    private static final String DRIVERORDERSURL = SERVERURL + "orders_asigned";
    private static final String UPDATEORDERURL = SERVERURL + "update_order";
    private static final String UPLOADPODURL = SERVERURL + "pod";

    /**
     *Method that calls the Torch login web service. It returns a JSONObject with
     * the answer: {@code "status":string,"driver_id":int,"driver_name":string}
     * @param user Username from the login form
     * @param psswd Password from the login form
     * @return A JSONObject with the answer from the server.
     * @since 2016.26.04
     */
    //TODO: Add the exceptions (method and documentation)
    public static JSONObject requestDriverLogin(String user, String psswd){
        String rqContent = "{\"user\":\"" + user + "\",\"password\":\"" + psswd + "\"}";
        return getJSONResponse(DRIVERLOGINURL,rqContent);
    }

    /**
     *Method that calls the Torch driver's orders service. It returns a JSONObject with
     * the answer including all orders and shipments
     * @param driverID The driver ID stored in the Shared Preferences.
     * @return A JSONObject with the answer from the server.
     * @since 2016.10.05
     */
    //TODO: Add the exceptions (method and documentation)
    public static JSONObject requestDriverOrders(int driverID){
        String rqContent = "{\"id\":\"" + driverID + "\"}";
        return getJSONResponse(DRIVERORDERSURL,rqContent);
    }

    /**
     *Method that calls the Torch update order status service. It returns a JSONObject with
     * the answer: {@code "status":string,"message":string}
     * @param driverID The driver ID stored in the Shared Preferences.
     * @param orderID The ID for the affected order
     * @param orderStatus The status to assign to the order
     * @return A JSONObject with the answer from the server.
     * @since 2016.06.05
     */
    //TODO: Add the exceptions (method and documentation)
    public static JSONObject updateOrderStatus(int driverID, int orderID, int orderStatus){
        String rqContent = "{\"driver_id\":\"" + driverID + "\",\"order_id\":\"" + orderID +
                           "\",\"status\":\"" + orderStatus + "\"}";
        return getJSONResponse(UPDATEORDERURL,rqContent);
    }

    /**
     *Method that calls the Torch upload Proof of Delivery service. It should return a JSONObject with
     * the answer: {@code "status":string,"message":string}, but currently it only sends the PODs
     * @param shipmentID The shipment ID of the POD to upload.
     * @param imgB64 The Base64 string for the image
     * @since 2016.06.05
     */
    //TODO: Add the exceptions (method and documentation)
    public static void uploadPOD(int shipmentID, String imgB64){
        String rqContent = "{\"id\":\"" + shipmentID + "\",\"image\":\"" + imgB64 + "\"}";
        getJSONPODResponse(UPLOADPODURL,rqContent);
        //return getJSONPODResponse(UPLOADPODURL,rqContent);
    }

    /**
     * Method that performs the HTTP connection to the Torch backend.
     * <br><br>
     *     It uses {@code HttpUrlConnection} instead of HttpClient because the latter is being
     *     phased out.
     * @param connectionURL The URL for the webservice
     * @param requestContent The content of the request
     * @return A JSONObject containing the response from the server
     * @since 2016.10.05
     */
    private static JSONObject getJSONResponse(String connectionURL, String requestContent){
        disableConnectionReuseIfNecessary();
        HttpURLConnection urlConnection = null;
        //To avoid problems with HttpUrlConnection
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        try {
            // create connection
            URL urlToRequest = new URL(connectionURL);
            urlConnection = (HttpURLConnection) urlToRequest.openConnection();
            urlConnection.setConnectTimeout(0);
            urlConnection.setReadTimeout(30000);
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Accept", "application/json");
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.setDoOutput(true);

            //Send the request
            DataOutputStream dStream = new DataOutputStream(urlConnection.getOutputStream());
            dStream.writeBytes(requestContent);
            dStream.flush(); // Flushes the data output stream.
            dStream.close(); // Closing the output stream.

            // create JSONObject from content
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String resp =getResponseText(in).replaceAll("\\r\\n|\\r|\\n", "");
            return new JSONObject(resp);

        } catch (Exception e){
            Log.e("conex", (e.getMessage()==null)? "Connection error":e.getMessage());
            e.printStackTrace();
        } finally {
            //close the connection
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return null;
    }

    //TODO Find a way to work with return responses and Asynctask
    /**
     * Method that performs a HTTP connection to the Torch POD upload web service. This is different
     * from {@code getJSONResponse} because it formats the {@code requestContent} using a JSONObject
     * and it doesn't return the server response, to avoid problems with the Asynctask.
     * <br><br>
     *     It uses {@code HttpUrlConnection} instead of HttpClient because the latter is being
     *     phased out.
     * @param connectionURL The URL for the webservice
     * @param requestContent The content of the request
     * @since 2016.06.06
     */
    private static void getJSONPODResponse(String connectionURL, String requestContent){
        disableConnectionReuseIfNecessary();
        HttpURLConnection urlConnection = null;
        JSONObject rq = null;

        //Added this to manage the Base64 long string
        try {
            rq = new JSONObject(requestContent);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //To avoid problems with HttpUrlConnection
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        try {
            // create connection
            URL urlToRequest = new URL(connectionURL);
            urlConnection = (HttpURLConnection) urlToRequest.openConnection();
            urlConnection.setConnectTimeout(0);
            urlConnection.setReadTimeout(30000);
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Accept", "application/json");
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.setDoOutput(true);
            urlConnection.setChunkedStreamingMode(1024);

            //Send the request
            DataOutputStream dStream = new DataOutputStream(urlConnection.getOutputStream());
            if (rq != null) {
                dStream.writeBytes(rq.toString());
            }
            dStream.flush(); // Flushes the data output stream.
            dStream.close(); // Closing the output stream.

            // create JSONObject from content
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String resp =getResponseText(in).replaceAll("\\r\\n|\\r|\\n", "");
            Log.e("resp", resp+" ");
            //return new JSONObject(resp);

        } catch (Exception e){
            Log.e("conex", (e.getMessage()==null)? "Connection error":e.getMessage());
            e.printStackTrace();
        } finally {
            //close the connection
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        //return null;
    }

    /**
     * Required in order to prevent issues in earlier Android version.
     * @since 2016.26.04
     */
    private static void disableConnectionReuseIfNecessary() {
        // see HttpURLConnection API doc
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.FROYO) {
            System.setProperty("http.keepAlive", "false");
        }
    }

    /**
     * Method to pre-process the response from the web service. Maybe this is not necessary
     * @param inStream Input stream of data to process
     * @return Processed string with the response
     * @since 2016.26.04
     */
    private static String getResponseText(InputStream inStream) {
        // very nice trick from
        // http://weblogs.java.net/blog/pat/archive/2004/10/stupid_scanner_1.html
        return new Scanner(inStream).useDelimiter("\\A").next();
    }
}